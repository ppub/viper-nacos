package nacos

import (
	"bytes"
	"errors"
	"io"

	//crypt "github.com/sagikazarmark/crypt/config"
	"github.com/spf13/viper"
)

func SetOptions(option *Option) {
	manager, _ := NewNacosConfigManager(option)

	viper.SupportedRemoteProviders = []string{"nacos"}
	viper.RemoteConfig = &remoteConfigProvider{ConfigManager: manager}
}

type remoteConfigProvider struct {
	ConfigManager *nacosConfigManager
}


func (rc *remoteConfigProvider) Get(rp viper.RemoteProvider) (io.Reader, error) {
	cm, err := rc.getConfigManager(rp)
	if err != nil {
		return nil, err
	}

	b, err := cm.Get(rp.Path())
	if err != nil {
		return nil, err
	}

	return bytes.NewReader(b), nil
}

func (rc *remoteConfigProvider) Watch(rp viper.RemoteProvider) (io.Reader, error) {
	return rc.Get(rp)
}

func (rc *remoteConfigProvider) WatchChannel(rp viper.RemoteProvider) (<-chan *viper.RemoteResponse, chan bool) {
	cm, err := rc.getConfigManager(rp)
	if err != nil {
		return nil, nil
	}

	quit := make(chan bool)
	quitwc := make(chan bool)

	viperResponseCh := cm.Watch(rp.Path(), quit)

	// need this function to convert the Channel response form crypt.Response to viper.Response
	go func (quitwc <-chan bool, quit chan<- bool) {
		for {
			select {
			case <- quitwc:
				quit <- true
				return
			}
		}
	}(quitwc, quit)

	/*
	switch cm := cmt.(type) {
	case viperConfigManager:
		quit := make(chan bool)
		viperResponseCh := cm.Watch("dataId", quit)
		return viperResponseCh, quit
	}
	*/

	return viperResponseCh, quitwc
}


func (rc *remoteConfigProvider) getConfigManager(rp viper.RemoteProvider) (*nacosConfigManager, error) {
	if rp.Provider() == "nacos" {
		return rc.ConfigManager, nil
	} else {
		return nil, errors.New("The Nacos configuration manager is not supported.")
	}
}

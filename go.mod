module gitlab.com/ppub/viper-nacos

go 1.16

require (
	github.com/nacos-group/nacos-sdk-go/v2 v2.0.0
	github.com/sagikazarmark/crypt v0.4.0
	github.com/spf13/viper v1.10.1
)

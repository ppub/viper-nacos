package nacos

type Option struct {
	Url string
	Port uint64
	NamespaceId string
	GroupName string
	LogDir string
	LogLevel string
	TimeoutMs uint64
}

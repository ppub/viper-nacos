package nacos

import (
	"errors"
	"strings"
	"log"

	"github.com/nacos-group/nacos-sdk-go/v2/clients"
	"github.com/nacos-group/nacos-sdk-go/v2/clients/config_client"
	"github.com/nacos-group/nacos-sdk-go/v2/common/constant"
	"github.com/nacos-group/nacos-sdk-go/v2/vo"

	"github.com/spf13/viper"

)

type nacosConfigManager struct {
	client config_client.IConfigClient
	option *Option
}

func NewNacosConfigManager(option *Option) (*nacosConfigManager, error) {
	var serverConfigs []constant.ServerConfig
	urls := strings.Split(option.Url, ";")
	for _, url := range urls {
		serverConfigs = append(serverConfigs, constant.ServerConfig{
			IpAddr: url,
			Port: option.Port,
		})
	}

	clientConfig := constant.ClientConfig{
		NamespaceId: option.NamespaceId,
		LogDir: option.LogDir,
		LogLevel: option.LogLevel,
		TimeoutMs: option.TimeoutMs,
		NotLoadCacheAtStart: true,
	}

	client, err := clients.NewConfigClient(vo.NacosClientParam{
		ClientConfig: &clientConfig,
		ServerConfigs: serverConfigs,
	})
	if err != nil {
		return nil, err
	}

	manager := &nacosConfigManager{
		client: client,
		option: option,
	}

	return manager, nil
}

func (cm *nacosConfigManager) Get(key string) ([]byte, error) {
	content, err := cm.client.GetConfig(vo.ConfigParam{
		DataId: key,
		Group: cm.option.GroupName,
	})

	if len(content) == 0 {
		return nil, errors.New("Fetch config failed with DataId: " + key)
	}

	return []byte(content), err
}

func (cm *nacosConfigManager) Watch(key string, stop chan bool) <-chan *viper.RemoteResponse {
	resp := make(chan *viper.RemoteResponse)

	configParams := vo.ConfigParam{
		DataId: key,
		Group: cm.option.GroupName,
		OnChange: func(namespace, group, dataId, data string) {
			resp <- &viper.RemoteResponse{
				Value: []byte(data),
				Error: nil,
			}
		},
	}

	err := cm.client.ListenConfig(configParams)
	if err != nil {
		log.Println("ListenConfig error occured, msg: ", err)

		return nil
	}

	go func() {
		for {
			select {
			case <-stop:
				_ = cm.client.CancelListenConfig(configParams)
				return
			}
		}
	}()

	return resp
}

package main

import (
	"fmt"

	remote "gitlab.com/ppub/viper-nacos"
	"github.com/spf13/viper"
)

var base, baseConf, dbConf *viper.Viper


func main() {
	base = viper.New()

	base.SetConfigFile("./nacos.toml")
	err := base.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Read base conf failed, msg: %s \n", err))
	}

	remote.SetOptions(&remote.Option{
		Url:         base.GetString("nacos.url"),            // nacos server 多地址需要地址用;号隔开，如 Url: "loc1;loc2;loc3"
		Port:        base.GetUint64("nacos.port"),                     // nacos server端口号
		NamespaceId: base.GetString("nacos.namespaceId"),               // nacos namespace
		GroupName:   base.GetString("nacos.groupName"),        // nacos group
		LogDir: base.GetString("nacos.logDir"),
		LogLevel: base.GetString("nacos.logLevel"),
		TimeoutMs: base.GetUint64("nacos.timeoutMs"),
	})

	// alternatively, you can create a new viper instance.
	baseConf = viper.New()

	baseConf.AddRemoteProvider("nacos", base.GetString("nacos.url"), base.GetString("base.name"))
	baseConf.SetConfigType("yaml") // because there is no file extension in a stream of bytes, supported extensions are "json", "toml", "yaml", "yml", "properties", "props", "prop", "env", "dotenv"

	// read from remote config the first time.
	err = baseConf.ReadRemoteConfig()
	if err != nil {
		fmt.Errorf("Read Remtoe Config failed, err: ", err)
	}

	fmt.Printf("Got base config, username: %s \n", baseConf.AllSettings())

	err = baseConf.WatchRemoteConfigOnChannel()
	if err != nil {
		fmt.Errorf("unable to read remote config: %v", err)
	}



	// alternatively, you can create a new viper instance.
	dbConf = viper.New()

	dbConf.AddRemoteProvider("nacos", base.GetString("nacos.url"), base.GetString("db.name"))
	dbConf.SetConfigType("yaml") // because there is no file extension in a stream of bytes, supported extensions are "json", "toml", "yaml", "yml", "properties", "props", "prop", "env", "dotenv"

	// read from remote config the first time.
	err = dbConf.ReadRemoteConfig()
	if err != nil {
		fmt.Errorf("Read Remtoe Config failed, err: ", err)
	}

	fmt.Printf("Got db config, username: %s \n", dbConf.AllSettings())

	err = dbConf.WatchRemoteConfigOnChannel()
	//err = newViper.WatchRemoteConfig()
	if err != nil {
		fmt.Errorf("unable to read remote config: %v", err)
	}
}
